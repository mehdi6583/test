import { SET_CURRENT_PAGE, SET_SIDEBAR } from "./types";

export const setCurrentPage = (page) => async (dispatch) => {
  dispatch({ type: SET_CURRENT_PAGE, payload: page });
};

export const setSidebar = (condition) => async (dispatch) => {
  dispatch({ type: SET_SIDEBAR, payload: condition });
};
