import React, { Component } from "react";
import { Container, Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import "./Navbar.css";
import colors from "../../../Constants/colors";
import logo from "../../../Assets/images/hampa-logo.png";
import { connect } from "react-redux";
import { setCurrentPage, setSidebar } from "../../../Actions/authActions";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import HamburgerMenu from "react-hamburger-menu";
import Menu from "./Menu";
import { Row, Col } from "antd";

import {
  CodeOutlined,
  MonetizationOn,
  PermDataSetting,
  Gavel,
  Info,
  AccountTree,
  AccountCircle,
  WorkOff,
  Work,
} from "@material-ui/icons";
import { Menu as Menu2, Dropdown, Popover } from "antd";
import { DownOutlined, CiCircleFilled } from "@ant-design/icons";

const content = (
  <div style={{ backgroundColor: "FFFFFF", direction: "rtl", padding: "10px" }}>
    <Grid container>
      <Grid item>
        <p
          style={{
            color: "black",
            fontFamily: "IRANSans",
            textAlign: "right",
            marginBottom: "8px",

            fontSize: "14px",
          }}
        >
          <svg width="20" height="10">
            <g>
              <circle cx="10" cy="5" r="4"></circle>
            </g>
          </svg>
          البوم های ما
        </p>
        <p
          className="popover-item1"
          style={{
            color: "grey",
            textAlign: "right",
            fontFamily: "IRANSans",
            cursor: "pointer",

            margin: "5px",

            marginRight: "20px",

            fontSize: "14px",
          }}
        >
          <span>خانه</span>
        </p>
        <p
          className="popover-item1"
          style={{
            color: "grey",
            cursor: "pointer",
            fontFamily: "IRANSans",
            textAlign: "right",
            margin: "5px",

            marginRight: "20px",

            fontSize: "14px",
          }}
        >
          <span>کمپین ها</span>
        </p>
        <p
          className="popover-item1"
          style={{
            color: "grey",

            fontFamily: "IRANSans",
            textAlign: "right",
            cursor: "pointer",

            fontSize: "14px",
            margin: "5px",

            marginRight: "20px",
          }}
        >
          <span>گالری</span>
        </p>
      </Grid>
      <Grid
        item
        style={{
          marginRight: "70px",
          paddingRight: "70px",
          paddingLeft: "70px",
          borderRight: "0.5px solid lightgray",
        }}
      >
        <p
          style={{
            color: "black",
            textAlign: "right",
            fontFamily: "IRANSans",
            fontSize: "14px",
            marginBottom: "8px",
          }}
        >
          <svg width="20" height="10">
            <g>
              <circle cx="10" cy="5" r="4"></circle>
            </g>
          </svg>
          ویدیوهای ما{" "}
        </p>
        <p
          className="popover-item1"
          style={{
            color: "grey",
            fontFamily: "IRANSans",
            textAlign: "right",
            margin: "5px",
            cursor: "pointer",

            marginRight: "20px",

            fontSize: "14px",
          }}
        >
          <span>کمپین ها</span>
        </p>

        <p
          className="popover-item1"
          style={{
            color: "grey",
            fontFamily: "IRANSans",
            textAlign: "right",
            margin: "5px",
            marginRight: "20px",
            cursor: "pointer",

            fontSize: "14px",
          }}
        >
          <span>درباره ی ما</span>
        </p>
      </Grid>
    </Grid>
  </div>
);

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
  }

  handleClick() {
    this.props.setSidebar(!this.props.auth.sidebarIsOpen);
  }
  render() {
    return (
      <div
        className="site-header"
        style={{
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        <Container
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          <Row
            style={{
              width: "100%",
            }}
          >
            <Col
              span={4}
              offset={2}
              style={{
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
              }}
            >
              <h1
                style={{
                  fontFamily: "Righteous",
                  color: colors.textColorPDark,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                hampa
              </h1>
            </Col>
            <Col span={16} className="menu">
              <ul className="list-reset" style={{ marginRight: "0px" }}>
                <li style={{ marginTop: "5px" }} className="anim">
                  <Link
                    style={{
                      margin: "0px",
                      padding: "0px",
                      display: "flex",
                      justifyContent: "flex-end",
                      alignItems: "flex-end",
                    }}
                  >
                    <Button
                      endIcon={
                        <Info
                          className="anim2"
                          style={{
                            marginRight: "10px",
                            transition: "all 0.5s linear",
                          }}
                        />
                      }
                      fullWidth
                      disabled
                      style={{
                        color: "darkslategrey",
                        //borderRadius: "25px",
                        backgroundColor: "transparent",
                        fontFamily: "IRANSans",
                        fontSize: "16px",
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "flex-end",
                      }}
                    >
                      <h9
                        style={{
                          color: colors.textColorPDark,
                          fontSize: "16px",
                        }}
                      >
                        درباره ی ما
                      </h9>{" "}
                    </Button>
                  </Link>
                </li>
                <li style={{ marginTop: "5px" }} className="anim">
                  <Link
                    style={{
                      margin: "0px",
                      padding: "0px",
                      display: "flex",
                      justifyContent: "flex-end",
                      alignItems: "flex-end",
                    }}
                  >
                    <Button
                      disabled
                      endIcon={
                        <AccountCircle
                          className="anim2"
                          style={{
                            marginRight: "10px",
                            transition: "all 0.5s linear",
                          }}
                        />
                      }
                      fullWidth
                      style={{
                        color: "darkslategrey",
                        backgroundColor: "transparent",
                        //borderRadius: "25px",
                        fontFamily: "IRANSans",
                        fontSize: "16px",

                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "flex-end",
                      }}
                    >
                      <h9
                        style={{
                          color: colors.textColorPDark,
                          fontSize: "16px",
                        }}
                      >
                        تماس با ما
                      </h9>{" "}
                    </Button>
                  </Link>
                </li>{" "}
                <li style={{ marginTop: "5px" }} className="anim">
                  <Popover content={content} placement="bottomRight">
                    <Link
                      style={{
                        margin: "0px",
                        padding: "0px",
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "flex-end",
                      }}
                    >
                      <Button
                        disabled
                        endIcon={
                          <Gavel
                            className="anim2"
                            style={{
                              marginRight: "10px",
                              transition: "all 0.5s linear",
                            }}
                          />
                        }
                        fullWidth
                        style={{
                          color: "darkslategrey",
                          backgroundColor: "transparent",
                          //borderRadius: "25px",
                          fontFamily: "IRANSans",
                          fontSize: "21px",

                          display: "flex",
                          justifyContent: "flex-end",
                          alignItems: "flex-end",
                        }}
                      >
                        {" "}
                        <h9
                          style={{
                            color: "darkslategrey",
                            fontSize: "16px",
                          }}
                        >
                          گالری
                        </h9>
                      </Button>
                    </Link>{" "}
                  </Popover>
                </li>
                <li style={{ marginTop: "5px" }} className="anim">
                  <Link
                    style={{
                      margin: "0px",
                      padding: "0px",
                      display: "flex",
                      justifyContent: "flex-end",
                      alignItems: "flex-end",
                    }}
                    onClick={async () => {
                      await this.props.setCurrentPage("campains");
                    }}
                    to="/campains"
                  >
                    <Button
                      fullWidth
                      disabled
                      endIcon={
                        <Work
                          className="anim2"
                          style={{
                            marginRight: "10px",
                            transition: "all 0.5s linear",
                          }}
                        />
                      }
                      style={{
                        color:
                          this.props.auth.currentPage === "campains"
                            ? "#c54b6c"
                            : "darkslategrey",
                        backgroundColor: "transparent",
                        //borderRadius: "25px",
                        fontFamily: "IRANSans",
                        fontSize: "16px",
                        transition: "all 0.5s linear",
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "flex-end",
                      }}
                    >
                      {" "}
                      <h9
                        style={{
                          color:
                            this.props.auth.currentPage === "campains"
                              ? "#c54b6c"
                              : "darkslategrey",
                          fontSize: "16px",
                          transition: "all 0.5s linear",
                        }}
                      >
                        کمپین ها
                      </h9>{" "}
                    </Button>
                  </Link>
                </li>{" "}
                <li style={{ marginTop: "5px" }} className="anim">
                  <Link
                    style={{
                      margin: "0px",
                      padding: "0px",
                      display: "flex",
                      justifyContent: "flex-end",
                      alignItems: "flex-end",
                    }}
                    onClick={async () => {
                      await this.props.setCurrentPage("home");
                    }}
                    to="/"
                  >
                    {" "}
                    <Button
                      disabled
                      endIcon={
                        <AccountTree
                          className="anim2"
                          style={{
                            marginRight: "10px",
                            transition: "all 0.5s linear",
                          }}
                        />
                      }
                      fullWidth
                      style={{
                        color:
                          this.props.auth.currentPage === "home"
                            ? "#c54b6c"
                            : "darkslategrey",
                        backgroundColor: "transparent",
                        //borderRadius: "25px",
                        fontFamily: "IRANSans",
                        fontSize: "16px",
                        transition: "all 0.5s linear",
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "flex-end",
                      }}
                    >
                      <h9
                        style={{
                          color:
                            this.props.auth.currentPage === "home"
                              ? "#c54b6c"
                              : "darkslategrey",
                          fontSize: "16px",
                          transition: "all 0.5s linear",
                        }}
                      >
                        خانه
                      </h9>{" "}
                    </Button>{" "}
                  </Link>
                </li>
              </ul>
            </Col>
            <Col span={16} className="hamburger-menu">
              <div>
                <HamburgerMenu
                  isOpen={this.props.auth.sidebarIsOpen}
                  menuClicked={this.handleClick.bind(this)}
                  width={30}
                  height={25}
                  strokeWidth={3}
                  rotate={0}
                  color="darkslategrey"
                  borderRadius={0}
                  animationDuration={0.5}
                />
              </div>
            </Col>
          </Row>
          <div className="sidebar-menu">
            <div
              style={{
                display:
                  this.props.auth.sidebarIsOpen === true ? "block" : "none",
                flexDirection: "column",
                position: "absolute",
                right: "0",
                top: "70px",
                zIndex: "9999",
                overflow: "hidden",
                transition: "max-height 0.25s ease-in-out, opacity 0.15s",
              }}
            >
              <Menu />
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

Navbar.propTypes = {
  auth: PropTypes.object.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  setSidebar: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default withRouter(
  connect(mapStateToProps, { setCurrentPage, setSidebar })(Navbar)
);
