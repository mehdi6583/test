import { SET_CURRENT_PAGE, SET_SIDEBAR } from "../Actions/types";

const initialState = {
  currentPage: "home",
  sidebarIsOpen: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.payload,
      };

    case SET_SIDEBAR:
      return {
        ...state,
        sidebarIsOpen: action.payload,
      };
    default:
      return state;
  }
};

export default authReducer;
