import { colors } from "@material-ui/core";
const mainColor = "#5658DD";
const mainColor2 = "purple";

// export default {
//   white: "white",
//   bgColorBlue: "#5658DD",
//   bgColorBlue2: "#6163FF",
//   bgColorMain: "#151719",
//   bgColorBtn: mainColor2,
//   bgColorNavbarBtn: "#151719",
//   bgColorNavbar: "#151719",
//   twitterIconColor: "#5dbcd2",
//   facebookIconColor: "#5658DD",
//   instagramIconColor: "purple",
//   youtubeIconColor: "red",
//   textColorB: mainColor,
//   textColorP: "#9CA9B3",
//   textColorPDark: "#717D86",
//   textColorH: "#ECEDED",
//   textColorBtn: mainColor,
//   borderColor: mainColor,
// };

export default {
  white: "white",
  bgColorBlue: "#5658DD",
  bgColorBlue2: "#6163FF",
  bgColorMain: "#151719",
  bgColorBtn: mainColor2,
  bgColorNavbarBtn: "#ffffff",
  bgColorNavbar: "#ffffff",
  twitterIconColor: "#5dbcd2",
  facebookIconColor: "#5658DD",
  instagramIconColor: "purple",
  youtubeIconColor: "red",
  textColorB: mainColor,
  textColorP: "#9CA9B3",
  textColorPDark: "darkslategrey",
  textColorH: "#ECEDED",
  textColorBtn: "#727369",
  borderColor: mainColor,
};
