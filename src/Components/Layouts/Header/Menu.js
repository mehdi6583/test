import React, { Component } from "react";
import { Container, Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import "./Navbar.css";
import colors from "../../../Constants/colors";
import logo from "../../../Assets/images/hampa-logo.png";
import { connect } from "react-redux";
import { setCurrentPage, setSidebar } from "../../../Actions/authActions";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import { DownOutlined } from "@ant-design/icons";
import { Popover } from "antd";

import {
  CodeOutlined,
  MonetizationOn,
  PermDataSetting,
  Gavel,
  Info,
  AccountTree,
  AccountCircle,
  WorkOff,
  Work,
} from "@material-ui/icons";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = { menuItem: "none" };
  }
  onClick() {
    if (this.state.menuItem == "none") {
      this.setState({ menuItem: "block" });
    } else {
      this.setState({ menuItem: "none" });
    }
  }
  render() {
    return (
      <ul className="list-reset header-nav-right menu-item">
        <li style={{ marginTop: "5px" }} className="anim">
          <Link
            style={{
              margin: "0px",
              padding: "0px",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
            onClick={async () => {
              await this.props.setCurrentPage("home");
              this.props.setSidebar(false);
              this.props.history.push("/");
            }}
          >
            <Button
              disabled
              endIcon={
                <AccountTree
                  className="anim2"
                  style={{
                    marginRight: "10px",
                    transition: "all 0.5s linear",
                  }}
                />
              }
              fullWidth
              style={{
                color:
                  this.props.auth.currentPage === "home"
                    ? "#c54b6c"
                    : "darkslategrey",
                backgroundColor: "transparent",
                //borderRadius: "25px",
                fontFamily: "IRANSans",
                fontSize: "16px",
                transition: "all 0.5s linear",
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "flex-end",
              }}
            >
              <h9
                style={{
                  color:
                    this.props.auth.currentPage === "home"
                      ? "#c54b6c"
                      : "darkslategrey",
                  fontSize: "16px",
                  transition: "all 0.5s linear",
                }}
              >
                خانه
              </h9>{" "}
            </Button>{" "}
          </Link>
        </li>{" "}
        <li style={{ marginTop: "5px" }} className="anim">
          <Link
            style={{
              margin: "0px",
              padding: "0px",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
            onClick={async () => {
              await this.props.setCurrentPage("campains");
              this.props.setSidebar(false);
              this.props.history.push("/campains");
            }}
          >
            <Button
              fullWidth
              disabled
              endIcon={
                <Work
                  className="anim2"
                  style={{
                    marginRight: "10px",
                    transition: "all 0.5s linear",
                  }}
                />
              }
              style={{
                color:
                  this.props.auth.currentPage === "campains"
                    ? "#c54b6c"
                    : "darkslategrey",
                backgroundColor: "transparent",
                //borderRadius: "25px",
                fontFamily: "IRANSans",
                fontSize: "16px",
                transition: "all 0.5s linear",
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "flex-end",
              }}
            >
              {" "}
              <h9
                style={{
                  color:
                    this.props.auth.currentPage === "campains"
                      ? "#c54b6c"
                      : "darkslategrey",
                  fontSize: "16px",
                  transition: "all 0.5s linear",
                }}
              >
                کمپین ها
              </h9>{" "}
            </Button>
          </Link>
        </li>{" "}
        <li style={{ marginTop: "5px" }} className="anim">
          <Link
            style={{
              margin: "0px",
              padding: "0px",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
            onClick={this.onClick.bind(this)}
          >
            <Button
              disabled
              endIcon={
                <Gavel
                  className="anim2"
                  style={{
                    marginRight: "10px",
                    transition: "all 0.5s linear",
                  }}
                />
              }
              fullWidth
              style={{
                color: "darkslategrey",
                backgroundColor: "transparent",
                //borderRadius: "25px",
                fontFamily: "IRANSans",
                fontSize: "21px",
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "flex-end",
              }}
            >
              {" "}
              <h9
                style={{
                  color: "darkslategrey",
                  fontSize: "16px",
                }}
              >
                گالری
              </h9>
            </Button>
          </Link>

          <ul
            className="list-reset header-nav-right menu-item2"
            style={{ display: this.state.menuItem }}
          >
            <li style={{ marginTop: "5px" }} className="anim">
              <Link
                style={{
                  margin: "0px",
                  padding: "0px",
                  display: "flex",
                  justifyContent: "flex-end",
                  alignItems: "flex-end",
                }}
                onClick={async () => {
                  await this.props.setCurrentPage("home");
                  this.props.setSidebar(false);
                  this.props.history.push("/");
                }}
              >
                <Button
                  disabled
                  // endIcon={
                  //   <AccountTree
                  //     className="anim2"
                  //     style={{
                  //       marginRight: "10px",
                  //       transition: "all 0.5s linear",
                  //     }}
                  //   />
                  // }
                  fullWidth
                  style={{
                    color:
                      this.props.auth.currentPage === "home"
                        ? "darkslategrey"
                        : "darkslategrey",
                    backgroundColor: "transparent",
                    //borderRadius: "25px",
                    fontFamily: "IRANSans",
                    fontSize: "16px",
                    transition: "all 0.5s linear",
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "flex-end",
                  }}
                >
                  <h9
                    style={{
                      color:
                        this.props.auth.currentPage === "home"
                          ? "darkslategrey"
                          : "darkslategrey",
                      fontSize: "16px",
                      transition: "all 0.5s linear",
                    }}
                  >
                    ایتم اول
                  </h9>{" "}
                </Button>{" "}
              </Link>
            </li>{" "}
            <li style={{ marginTop: "5px" }} className="anim">
              <Link
                style={{
                  margin: "0px",
                  padding: "0px",
                  display: "flex",
                  justifyContent: "flex-end",
                  alignItems: "flex-end",
                }}
                onClick={async () => {
                  await this.props.setCurrentPage("campains");
                  this.props.setSidebar(false);
                  this.props.history.push("/campains");
                }}
              >
                <Button
                  fullWidth
                  disabled
                  // endIcon={
                  //   <Work
                  //     className="anim2"
                  //     style={{
                  //       marginRight: "10px",
                  //       transition: "all 0.5s linear",
                  //     }}
                  //   />
                  //}
                  style={{
                    color:
                      this.props.auth.currentPage === "campains"
                        ? "darkslategrey"
                        : "darkslategrey",
                    backgroundColor: "transparent",
                    //borderRadius: "25px",
                    fontFamily: "IRANSans",
                    fontSize: "16px",
                    transition: "all 0.5s linear",
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "flex-end",
                  }}
                >
                  {" "}
                  <h9
                    style={{
                      color:
                        this.props.auth.currentPage === "campains"
                          ? "darkslategrey"
                          : "darkslategrey",
                      fontSize: "16px",
                      transition: "all 0.5s linear",
                    }}
                  >
                    ایتم دوم
                  </h9>{" "}
                </Button>
              </Link>
            </li>{" "}
          </ul>
        </li>
        <li style={{ marginTop: "5px" }} className="anim">
          <Link
            style={{
              margin: "0px",
              padding: "0px",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
          >
            <Button
              disabled
              endIcon={
                <AccountCircle
                  className="anim2"
                  style={{
                    marginRight: "10px",
                    transition: "all 0.5s linear",
                  }}
                />
              }
              fullWidth
              style={{
                color: "darkslategrey",
                backgroundColor: "transparent",
                //borderRadius: "25px",
                fontFamily: "IRANSans",
                fontSize: "16px",

                display: "flex",
                justifyContent: "flex-end",
                alignItems: "flex-end",
              }}
            >
              <h9
                style={{
                  color: colors.textColorPDark,
                  fontSize: "16px",
                }}
              >
                تماس با ما
              </h9>{" "}
            </Button>
          </Link>
        </li>{" "}
        <li style={{ marginTop: "5px" }} className="anim">
          <Link
            style={{
              margin: "0px",
              padding: "0px",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
          >
            <Button
              endIcon={
                <Info
                  className="anim2"
                  style={{
                    marginRight: "10px",
                    transition: "all 0.5s linear",
                  }}
                />
              }
              fullWidth
              disabled
              style={{
                color: "darkslategrey",
                //borderRadius: "25px",
                backgroundColor: "transparent",
                fontFamily: "IRANSans",
                fontSize: "16px",
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "flex-end",
              }}
            >
              <h9
                style={{
                  color: colors.textColorPDark,
                  fontSize: "16px",
                }}
              >
                درباره ی ما
              </h9>{" "}
            </Button>
          </Link>
        </li>
      </ul>
    );
  }
}

Menu.propTypes = {
  auth: PropTypes.object.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  setSidebar: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default withRouter(
  connect(mapStateToProps, { setCurrentPage, setSidebar })(Menu)
);
