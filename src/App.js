import React, { useRef, useEffect } from "react";
import Navbar from "./Components/Layouts/Header/Navbar";
import Users from "./Components/Pages/Users/Users";
import Profile from "./Components/Pages/Profile/Profile";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "./App.css";

const App = () => {
  return (
    <div style={{ fontFamily: "IRANSans", minWidth: "500px" }}>
      <Router>
        <Navbar />

        <Switch>
          <Route exact path="/" component={Users} />
          <Route exact path="/:profile" component={Profile} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
