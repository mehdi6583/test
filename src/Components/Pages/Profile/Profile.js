import React, { Component } from "react";
import { Container } from "@material-ui/core";
import { TableCell, Grid } from "@material-ui/core";
import TableRow from "@material-ui/core/TableRow";
import f1 from "../../../Assets/images/1.png";
import f2 from "../../../Assets/images/2.png";
import f3 from "../../../Assets/images/3.png";
import f4 from "../../../Assets/images/4.png";
import Paper from "@material-ui/core/Paper";
import { Row, Col, Divider } from "antd";

import {
  Chart,
  ArgumentAxis,
  ValueAxis,
  LineSeries,
  Title,
  Legend,
} from "@devexpress/dx-react-chart-material-ui";
import { withStyles } from "@material-ui/core/styles";
import { Animation } from "@devexpress/dx-react-chart";

const data = [
  {
    trade: 1,
    cost: 132,
  },
  {
    trade: 2,
    cost: 256,
  },
  {
    trade: 3,
    cost: 100,
  },
  {
    trade: 4,
    cost: 200,
  },
  {
    trade: 5,
    cost: 120,
  },
  {
    trade: 6,
    cost: 146,
  },
  {
    trade: 7,
    cost: 156,
  },
  {
    trade: 8,
    cost: 156,
  },
  {
    trade: 9,
    cost: 196,
  },
  {
    trade: 10,
    cost: 256,
  },
  {
    trade: 11,
    cost: 256,
  },
  {
    trade: 12,
    cost: 132,
  },
  {
    trade: 13,
    cost: 156,
  },
  {
    trade: 14,
    cost: 156,
  },
  {
    trade: 15,
    cost: 196,
  },
  {
    trade: 16,
    cost: 256,
  },
  {
    trade: 17,
    cost: 256,
  },
  {
    trade: 18,
    cost: 256,
  },
  {
    trade: 19,
    cost: 100,
  },
  {
    trade: 20,
    cost: 200,
  },
];

const ValueLabel = (props) => {
  const { text } = props;
  return <ValueAxis.Label {...props} text={`${text}`} />;
};

const titleStyles = {
  title: {
    whiteSpace: "pre",
  },
};

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      user: "",
      userData: [
        {
          id: 1,
          image: f1,
          name: "حسین حسینی",
          return: 0.5,
          risk: 6,
          color: "green",
        },
        {
          id: 10,
          image: f2,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 9,
          image: f4,
          name: "پویا حسینی",
          return: 1.5,
          risk: 5,
          color: "green",
        },
        {
          id: 8,
          image: f1,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 7,
          image: f4,
          name: "مهدی غلامی",
          return: 2.5,
          risk: 5,
          color: "green",
        },
        {
          id: 6,
          image: f2,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 5,
          image: f1,
          name: "حسین حسینی",
          return: 2.5,
          risk: 5,
          color: "green",
        },
        {
          id: 4,
          image: f1,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 3,
          image: f4,
          name: "حسین حسینی",
          return: 2.5,
          risk: 5,
          color: "green",
        },
        {
          id: 2,
          image: f3,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
      ],
      data,
    };
  }
  componentDidMount() {
    const { profile } = this.props.match.params;
    this.setState({ name: profile });
    this.state.userData.forEach((data) => {
      if (data.id == profile) this.setState({ user: data });
    });
  }

  render() {
    const { data: chartData } = this.state;

    return (
      <Container>
        <Grid container>
          <Grid item xs={12} md={8} style={{ padding: "20px" }}>
            <div style={{ width: "100%", marginBottom: "30px" }}>
              <Paper>
                <Chart data={chartData}>
                  <ArgumentAxis />
                  <ValueAxis max={50} labelComponent={ValueLabel} />

                  <LineSeries
                    name="cost"
                    valueField="cost"
                    argumentField="trade"
                  />

                  <Legend position="bottom" />
                  <Title text={"your trades"} />
                  <Animation />
                </Chart>
              </Paper>
            </div>
          </Grid>
          <Grid xs={12} md={4} style={{ padding: "20px" }}>
            <div
              style={{
                width: "100%",
                padding: "10px",
                marginTop: "20px",
                backgroundColor: "#ffffff",
                paddingBottom: "40px",
                border: "2px solid #dfdfdf",
                direction: "rtl",
              }}
            >
              <div style={{ width: "100%" }}>
                <ul
                  style={{
                    width: "100%",
                    listStyle: "none",
                    paddingLeft: "0px",
                    fontFamily: "news",
                  }}
                >
                  <li
                    style={{
                      width: "100%",
                      height: "50px",
                      paddingLeft: "10px",
                      borderBottom: "1px solid #efefef",
                    }}
                  >
                    <span
                      style={{
                        display: "flex",
                        alignItems: "center",
                        height: "50px",
                        fontFamily: "news3",
                      }}
                    >
                      <img
                        height="40px"
                        with="40px"
                        src={this.state.user.image}
                        style={{ marginLeft: "5px" }}
                      />
                    </span>
                  </li>

                  <li
                    style={{
                      width: "100%",
                      borderBottom: "1px solid #efefef",
                      height: "50px",
                      paddingLeft: "10px",
                    }}
                  >
                    <span
                      style={{
                        float: "right",
                        height: "50px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        width: "50%",
                        fontFamily: "news2",
                        color: "#ff3786",
                      }}
                    >
                      مشتری :
                    </span>
                    <span
                      style={{
                        display: "flex",
                        alignItems: "center",
                        height: "50px",
                        fontFamily: "Righteous",
                        fontFamily: "news2",
                      }}
                    >
                      {this.state.user.name}{" "}
                    </span>
                  </li>

                  <li
                    style={{
                      width: "100%",
                      borderBottom: "1px solid #efefef",
                      height: "50px",
                      paddingLeft: "10px",
                    }}
                  >
                    <span
                      style={{
                        float: "right",
                        height: "50px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        width: "50%",
                        fontFamily: "news2",
                        color: "#ff3786",
                      }}
                    >
                      return
                    </span>
                    <span
                      style={{
                        display: "flex",
                        alignItems: "center",
                        height: "50px",
                        fontFamily: "Righteous",
                        fontFamily: "news2",
                      }}
                    >
                      {this.state.user.return}{" "}
                    </span>
                  </li>
                  <li
                    style={{
                      width: "100%",
                      borderBottom: "1px solid #efefef",
                      height: "50px",
                      paddingLeft: "10px",
                    }}
                  >
                    <span
                      style={{
                        float: "right",
                        height: "50px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        width: "50%",
                        fontFamily: "news2",
                        color: "#ff3786",
                      }}
                    >
                      risk
                    </span>
                    <span
                      style={{
                        display: "flex",
                        alignItems: "center",
                        height: "50px",
                        fontFamily: "Righteous",
                        fontFamily: "news2",
                      }}
                    >
                      {this.state.user.risk}{" "}
                    </span>
                  </li>
                  <li
                    style={{
                      width: "100%",
                      borderBottom: "1px solid #efefef",
                      height: "50px",
                      paddingLeft: "10px",
                    }}
                  >
                    <span
                      style={{
                        float: "right",
                        height: "50px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        width: "50%",
                        fontFamily: "news2",
                        color: "#ff3786",
                      }}
                    >
                      trades
                    </span>
                    <span
                      style={{
                        display: "flex",
                        alignItems: "center",
                        height: "50px",
                        fontFamily: "Righteous",
                        fontFamily: "news2",
                      }}
                    >
                      20
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>
    );
  }
}
