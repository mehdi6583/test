import React, { Component } from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Carousel from "react-elastic-carousel";
import f1 from "../../../Assets/images/1.png";
import f2 from "../../../Assets/images/2.png";
import f3 from "../../../Assets/images/3.png";
import f4 from "../../../Assets/images/4.png";
import { Container } from "@material-ui/core";
import "./Users.css";
import { Link } from "react-router-dom";

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          id: 1,
          image: f1,
          name: "حسین حسینی",
          return: 0.5,
          risk: 6,
          color: "green",
        },
        {
          id: 10,
          image: f2,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 9,
          image: f4,
          name: "پویا حسینی",
          return: 1.5,
          risk: 5,
          color: "green",
        },
        {
          id: 8,
          image: f1,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 7,
          image: f4,
          name: "مهدی غلامی",
          return: 2.5,
          risk: 5,
          color: "green",
        },
        {
          id: 6,
          image: f2,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 5,
          image: f1,
          name: "حسین حسینی",
          return: 2.5,
          risk: 5,
          color: "green",
        },
        {
          id: 4,
          image: f1,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
        {
          id: 3,
          image: f4,
          name: "حسین حسینی",
          return: 2.5,
          risk: 5,
          color: "green",
        },
        {
          id: 2,
          image: f3,
          name: "محمد نجفی",
          return: -5,
          risk: 5.3,
          color: "red",
        },
      ],
    };
  }
  render() {
    return (
      <div>
        {" "}
        <div
          style={{
            paddingTop: "70px",
            backgroundColor: "#f9fafd",
            paddingBottom: "70px",
          }}
        >
          {" "}
          <Container
            style={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
              maxWidth: "750px",
              backgroundColor: "#ffffff",
              padding: "30px",
              border: "0.5px solid #efefef",
              fontFamily: "iranSans",
            }}
          >
            گروه اول
          </Container>
          <Container
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              maxWidth: "750px",
              backgroundColor: "#ffffff",
              padding: "30px",
              border: "0.5px solid #efefef",
            }}
          >
            <Carousel itemsToShow={4}>
              {this.state.data.map((user) => (
                <Link style={{ cursor: "pointer" }} to={"" + user.id}>
                  <div
                    style={{
                      width: "100%",
                      backgroundColor: "#ffffff",
                    }}
                  >
                    <div style={{ width: "100%" }}>
                      <ul
                        style={{
                          width: "100%",
                          listStyle: "none",
                          fontFamily: "news",
                          textAlign: "center",
                          color: "black",
                          fontFamily: "iranSans",
                        }}
                        className="row"
                      >
                        <div style={{ padding: "5px" }}>
                          <li
                            style={{
                              width: "100%",
                              overflow: "hidden",
                            }}
                          >
                            <img height="45px" width="auto" src={user.image} />
                          </li>
                          <li
                            style={{
                              width: "100%",
                            }}
                          >
                            <div>{user.name}</div>
                          </li>
                          <li
                            style={{
                              width: "100%",
                            }}
                          >
                            <div> {user.risk}</div>
                          </li>
                          <li
                            style={{
                              width: "100%",
                              color: user.color,
                            }}
                          >
                            {" "}
                            <div> {user.return}</div>
                          </li>
                        </div>
                      </ul>
                    </div>
                  </div>
                </Link>
              ))}
            </Carousel>
          </Container>
        </div>
        <div
          style={{
            paddingTop: "70px",
            backgroundColor: "#f9fafd",
            paddingBottom: "70px",
          }}
        >
          {" "}
          <Container
            style={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
              maxWidth: "750px",
              backgroundColor: "#ffffff",
              padding: "30px",
              border: "0.5px solid #efefef",
              fontFamily: "iranSans",
            }}
          >
            گروه دوم
          </Container>
          <Container
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              maxWidth: "750px",
              backgroundColor: "#ffffff",
              padding: "30px",
              border: "0.5px solid #efefef",
            }}
          >
            <Carousel itemsToShow={4}>
              {this.state.data.map((user) => (
                <Link style={{ cursor: "pointer" }} to={"" + user.id}>
                  <div
                    style={{
                      width: "100%",
                      backgroundColor: "#ffffff",
                    }}
                  >
                    <div style={{ width: "100%" }}>
                      <ul
                        style={{
                          width: "100%",
                          listStyle: "none",
                          fontFamily: "news",
                          textAlign: "center",
                          color: "black",
                          fontFamily: "iranSans",
                        }}
                        className="row"
                      >
                        <div style={{ padding: "5px" }}>
                          <li
                            style={{
                              width: "100%",
                              overflow: "hidden",
                            }}
                          >
                            <img height="45px" width="auto" src={user.image} />
                          </li>
                          <li
                            style={{
                              width: "100%",
                            }}
                          >
                            <div>{user.name}</div>
                          </li>
                          <li
                            style={{
                              width: "100%",
                            }}
                          >
                            <div> {user.risk}</div>
                          </li>
                          <li
                            style={{
                              width: "100%",
                              color: user.color,
                            }}
                          >
                            {" "}
                            <div> {user.return}</div>
                          </li>
                        </div>
                      </ul>
                    </div>
                  </div>
                </Link>
              ))}
            </Carousel>
          </Container>
        </div>
      </div>
    );
  }
}
